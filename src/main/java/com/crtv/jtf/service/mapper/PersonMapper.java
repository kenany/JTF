package com.crtv.jtf.service.mapper;

import com.crtv.jtf.domain.*;
import com.crtv.jtf.service.dto.PersonDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Person and its DTO PersonDTO.
 */
@Mapper(componentModel = "spring", uses = {JournalMapper.class, ReportageMapper.class, RoleMapper.class})
public interface PersonMapper extends EntityMapper<PersonDTO, Person> {

    @Mapping(source = "journal.id", target = "journalId")
    @Mapping(source = "reportage.id", target = "reportageId")
    PersonDTO toDto(Person person); 

    @Mapping(source = "journalId", target = "journal")
    @Mapping(source = "reportageId", target = "reportage")
    Person toEntity(PersonDTO personDTO);

    default Person fromId(Long id) {
        if (id == null) {
            return null;
        }
        Person person = new Person();
        person.setId(id);
        return person;
    }
}
