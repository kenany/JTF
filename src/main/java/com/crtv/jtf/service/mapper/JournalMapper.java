package com.crtv.jtf.service.mapper;

import com.crtv.jtf.domain.*;
import com.crtv.jtf.service.dto.JournalDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Journal and its DTO JournalDTO.
 */
@Mapper(componentModel = "spring", uses = {PersonMapper.class})
public interface JournalMapper extends EntityMapper<JournalDTO, Journal> {

    @Mapping(source = "realisateur.id", target = "realisateurId")
    @Mapping(source = "chefediteur.id", target = "chefediteurId")
    @Mapping(source = "realisateur.name", target = "realisateurName")
    @Mapping(source = "chefediteur.name", target = "chefediteurName")
    JournalDTO toDto(Journal journal); 

    @Mapping(target = "repotabges", ignore = true)
    @Mapping(target = "presentateurs", ignore = true)
//    @Mapping(target = "realisateurName", ignore = true)
//    @Mapping(target = "chefediteurName", ignore = true)
    @Mapping(source = "realisateurId", target = "realisateur")
    @Mapping(source = "chefediteurId", target = "chefediteur")
    Journal toEntity(JournalDTO journalDTO);

    default Journal fromId(Long id) {
        if (id == null) {
            return null;
        }
        Journal journal = new Journal();
        journal.setId(id);
        return journal;
    }
}
