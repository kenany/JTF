package com.crtv.jtf.service.mapper;

import com.crtv.jtf.domain.*;
import com.crtv.jtf.service.dto.ReportageDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Reportage and its DTO ReportageDTO.
 */
@Mapper(componentModel = "spring", uses = {JournalMapper.class})
public interface ReportageMapper extends EntityMapper<ReportageDTO, Reportage> {

    @Mapping(source = "journal.id", target = "journalId")
    ReportageDTO toDto(Reportage reportage); 

    @Mapping(source = "journalId", target = "journal")
    @Mapping(target = "intervenants", ignore = true)
    @Mapping(target = "motscles", ignore = true)
    Reportage toEntity(ReportageDTO reportageDTO);

    default Reportage fromId(Long id) {
        if (id == null) {
            return null;
        }
        Reportage reportage = new Reportage();
        reportage.setId(id);
        return reportage;
    }
}
