package com.crtv.jtf.service.mapper;

import com.crtv.jtf.domain.*;
import com.crtv.jtf.service.dto.MotClesDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity MotCles and its DTO MotClesDTO.
 */
@Mapper(componentModel = "spring", uses = {ReportageMapper.class})
public interface MotClesMapper extends EntityMapper<MotClesDTO, MotCles> {

    @Mapping(source = "reportage.id", target = "reportageId")
    MotClesDTO toDto(MotCles motCles); 

    @Mapping(source = "reportageId", target = "reportage")
    MotCles toEntity(MotClesDTO motClesDTO);

    default MotCles fromId(Long id) {
        if (id == null) {
            return null;
        }
        MotCles motCles = new MotCles();
        motCles.setId(id);
        return motCles;
    }
}
