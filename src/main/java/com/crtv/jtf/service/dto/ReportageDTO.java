package com.crtv.jtf.service.dto;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Reportage entity.
 */
public class ReportageDTO implements Serializable {

    private Long id;

    private String titre;

    private String timeCode;

    private Long journalId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getTimeCode() {
        return timeCode;
    }

    public void setTimeCode(String timeCode) {
        this.timeCode = timeCode;
    }

    public Long getJournalId() {
        return journalId;
    }

    public void setJournalId(Long journalId) {
        this.journalId = journalId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReportageDTO reportageDTO = (ReportageDTO) o;
        if(reportageDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), reportageDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ReportageDTO{" +
            "id=" + getId() +
            ", titre='" + getTitre() + "'" +
            ", timeCode='" + getTimeCode() + "'" +
            "}";
    }
}
