package com.crtv.jtf.service.dto;


import java.time.LocalDate;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Journal entity.
 */
public class JournalDTO implements Serializable {

    private Long id;

    private String cote;

    private LocalDate date;

    private Integer duree;

    private String lieu;

    private String qualite;

    private String videoUrl;

    private Long realisateurId;

    private Long chefediteurId;
    
    private String realisateurName;

    private String chefediteurName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCote() {
        return cote;
    }

    public void setCote(String cote) {
        this.cote = cote;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Integer getDuree() {
        return duree;
    }

    public void setDuree(Integer duree) {
        this.duree = duree;
    }

    public String getLieu() {
        return lieu;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    public String getQualite() {
        return qualite;
    }

    public void setQualite(String qualite) {
        this.qualite = qualite;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public Long getRealisateurId() {
        return realisateurId;
    }

    public void setRealisateurId(Long personId) {
        this.realisateurId = personId;
    }

    public Long getChefediteurId() {
        return chefediteurId;
    }

    public void setChefediteurId(Long personId) {
        this.chefediteurId = personId;
    }

    public String getRealisateurName() {
        return realisateurName;
    }

    public void setRealisateurName(String realisateurName) {
        this.realisateurName = realisateurName;
    }

    public String getChefediteurName() {
        return chefediteurName;
    }

    public void setChefediteurName(String chefediteurName) {
        this.chefediteurName = chefediteurName;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        JournalDTO journalDTO = (JournalDTO) o;
        if(journalDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), journalDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "JournalDTO{" +
            "id=" + getId() +
            ", cote='" + getCote() + "'" +
            ", date='" + getDate() + "'" +
            ", duree='" + getDuree() + "'" +
            ", lieu='" + getLieu() + "'" +
            ", qualite='" + getQualite() + "'" +
            ", videoUrl='" + getVideoUrl() + "'" +
            "}";
    }
}
