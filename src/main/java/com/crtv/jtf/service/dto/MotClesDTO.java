package com.crtv.jtf.service.dto;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the MotCles entity.
 */
public class MotClesDTO implements Serializable {

    private Long id;

    private String mot;

    private Long reportageId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMot() {
        return mot;
    }

    public void setMot(String mot) {
        this.mot = mot;
    }

    public Long getReportageId() {
        return reportageId;
    }

    public void setReportageId(Long reportageId) {
        this.reportageId = reportageId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MotClesDTO motClesDTO = (MotClesDTO) o;
        if(motClesDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), motClesDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "MotClesDTO{" +
            "id=" + getId() +
            ", mot='" + getMot() + "'" +
            "}";
    }
}
