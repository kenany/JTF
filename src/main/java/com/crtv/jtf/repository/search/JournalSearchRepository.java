package com.crtv.jtf.repository.search;

import com.crtv.jtf.domain.Journal;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Journal entity.
 */
public interface JournalSearchRepository extends ElasticsearchRepository<Journal, Long> {
}
