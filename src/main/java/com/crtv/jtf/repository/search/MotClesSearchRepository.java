package com.crtv.jtf.repository.search;

import com.crtv.jtf.domain.MotCles;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the MotCles entity.
 */
public interface MotClesSearchRepository extends ElasticsearchRepository<MotCles, Long> {
}
