package com.crtv.jtf.repository.search;

import com.crtv.jtf.domain.Reportage;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Reportage entity.
 */
public interface ReportageSearchRepository extends ElasticsearchRepository<Reportage, Long> {
}
