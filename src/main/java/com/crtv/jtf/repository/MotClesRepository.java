package com.crtv.jtf.repository;

import com.crtv.jtf.domain.MotCles;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the MotCles entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MotClesRepository extends JpaRepository<MotCles, Long> {

}
