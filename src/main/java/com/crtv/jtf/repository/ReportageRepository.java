package com.crtv.jtf.repository;

import com.crtv.jtf.domain.Reportage;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Reportage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ReportageRepository extends JpaRepository<Reportage, Long> {

}
