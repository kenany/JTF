package com.crtv.jtf.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A MotCles.
 */
@Entity
@Table(name = "mot_cles")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "motcles")
public class MotCles implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "mot")
    private String mot;

    @ManyToOne
    private Reportage reportage;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMot() {
        return mot;
    }

    public MotCles mot(String mot) {
        this.mot = mot;
        return this;
    }

    public void setMot(String mot) {
        this.mot = mot;
    }

    public Reportage getReportage() {
        return reportage;
    }

    public MotCles reportage(Reportage reportage) {
        this.reportage = reportage;
        return this;
    }

    public void setReportage(Reportage reportage) {
        this.reportage = reportage;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MotCles motCles = (MotCles) o;
        if (motCles.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), motCles.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "MotCles{" +
            "id=" + getId() +
            ", mot='" + getMot() + "'" +
            "}";
    }
}
