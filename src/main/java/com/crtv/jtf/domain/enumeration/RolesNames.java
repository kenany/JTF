package com.crtv.jtf.domain.enumeration;

/**
 * The RolesNames enumeration.
 */
public enum RolesNames {
    REALISATEUR, PRESENTATEUR, CHEF_EDITEUR, ADMIN
}
