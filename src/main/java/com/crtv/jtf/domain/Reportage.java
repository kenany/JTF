package com.crtv.jtf.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Reportage.
 */
@Entity
@Table(name = "reportage")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "reportage")
public class Reportage implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "titre")
    private String titre;

    @Column(name = "time_code")
    private String timeCode;

    @ManyToOne
    private Journal journal;

    @OneToMany(mappedBy = "reportage")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Person> intervenants = new HashSet<>();

    @OneToMany(mappedBy = "reportage")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<MotCles> motscles = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public Reportage titre(String titre) {
        this.titre = titre;
        return this;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getTimeCode() {
        return timeCode;
    }

    public Reportage timeCode(String timeCode) {
        this.timeCode = timeCode;
        return this;
    }

    public void setTimeCode(String timeCode) {
        this.timeCode = timeCode;
    }

    public Journal getJournal() {
        return journal;
    }

    public Reportage journal(Journal journal) {
        this.journal = journal;
        return this;
    }

    public void setJournal(Journal journal) {
        this.journal = journal;
    }

    public Set<Person> getIntervenants() {
        return intervenants;
    }

    public Reportage intervenants(Set<Person> people) {
        this.intervenants = people;
        return this;
    }

    public Reportage addIntervenants(Person person) {
        this.intervenants.add(person);
        person.setReportage(this);
        return this;
    }

    public Reportage removeIntervenants(Person person) {
        this.intervenants.remove(person);
        person.setReportage(null);
        return this;
    }

    public void setIntervenants(Set<Person> people) {
        this.intervenants = people;
    }

    public Set<MotCles> getMotscles() {
        return motscles;
    }

    public Reportage motscles(Set<MotCles> motCles) {
        this.motscles = motCles;
        return this;
    }

    public Reportage addMotscles(MotCles motCles) {
        this.motscles.add(motCles);
        motCles.setReportage(this);
        return this;
    }

    public Reportage removeMotscles(MotCles motCles) {
        this.motscles.remove(motCles);
        motCles.setReportage(null);
        return this;
    }

    public void setMotscles(Set<MotCles> motCles) {
        this.motscles = motCles;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Reportage reportage = (Reportage) o;
        if (reportage.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), reportage.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Reportage{" +
            "id=" + getId() +
            ", titre='" + getTitre() + "'" +
            ", timeCode='" + getTimeCode() + "'" +
            "}";
    }
}
