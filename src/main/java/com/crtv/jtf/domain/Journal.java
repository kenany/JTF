package com.crtv.jtf.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Journal.
 */
@Entity
@Table(name = "journal")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "journal")
public class Journal implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "cote")
    private String cote;

    @Column(name = "jhi_date")
    private LocalDate date;

    @Column(name = "duree")
    private Integer duree;

    @Column(name = "lieu")
    private String lieu;

    @Column(name = "qualite")
    private String qualite;

    @Column(name = "video_url")
    private String videoUrl;

    @OneToMany(mappedBy = "journal")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Reportage> repotabges = new HashSet<>();

    @OneToMany(mappedBy = "journal")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Person> presentateurs = new HashSet<>();

    @ManyToOne
    private Person realisateur;

    @ManyToOne
    private Person chefediteur;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCote() {
        return cote;
    }

    public Journal cote(String cote) {
        this.cote = cote;
        return this;
    }

    public void setCote(String cote) {
        this.cote = cote;
    }

    public LocalDate getDate() {
        return date;
    }

    public Journal date(LocalDate date) {
        this.date = date;
        return this;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Integer getDuree() {
        return duree;
    }

    public Journal duree(Integer duree) {
        this.duree = duree;
        return this;
    }

    public void setDuree(Integer duree) {
        this.duree = duree;
    }

    public String getLieu() {
        return lieu;
    }

    public Journal lieu(String lieu) {
        this.lieu = lieu;
        return this;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    public String getQualite() {
        return qualite;
    }

    public Journal qualite(String qualite) {
        this.qualite = qualite;
        return this;
    }

    public void setQualite(String qualite) {
        this.qualite = qualite;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public Journal videoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
        return this;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public Set<Reportage> getRepotabges() {
        return repotabges;
    }

    public Journal repotabges(Set<Reportage> reportages) {
        this.repotabges = reportages;
        return this;
    }

    public Journal addRepotabges(Reportage reportage) {
        this.repotabges.add(reportage);
        reportage.setJournal(this);
        return this;
    }

    public Journal removeRepotabges(Reportage reportage) {
        this.repotabges.remove(reportage);
        reportage.setJournal(null);
        return this;
    }

    public void setRepotabges(Set<Reportage> reportages) {
        this.repotabges = reportages;
    }

    public Set<Person> getPresentateurs() {
        return presentateurs;
    }

    public Journal presentateurs(Set<Person> people) {
        this.presentateurs = people;
        return this;
    }

    public Journal addPresentateurs(Person person) {
        this.presentateurs.add(person);
        person.setJournal(this);
        return this;
    }

    public Journal removePresentateurs(Person person) {
        this.presentateurs.remove(person);
        person.setJournal(null);
        return this;
    }

    public void setPresentateurs(Set<Person> people) {
        this.presentateurs = people;
    }

    public Person getRealisateur() {
        return realisateur;
    }

    public Journal realisateur(Person person) {
        this.realisateur = person;
        return this;
    }

    public void setRealisateur(Person person) {
        this.realisateur = person;
    }

    public Person getChefediteur() {
        return chefediteur;
    }

    public Journal chefediteur(Person person) {
        this.chefediteur = person;
        return this;
    }

    public void setChefediteur(Person person) {
        this.chefediteur = person;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Journal journal = (Journal) o;
        if (journal.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), journal.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Journal{" +
            "id=" + getId() +
            ", cote='" + getCote() + "'" +
            ", date='" + getDate() + "'" +
            ", duree='" + getDuree() + "'" +
            ", lieu='" + getLieu() + "'" +
            ", qualite='" + getQualite() + "'" +
            ", videoUrl='" + getVideoUrl() + "'" +
            "}";
    }
}
