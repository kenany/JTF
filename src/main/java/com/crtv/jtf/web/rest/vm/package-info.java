/**
 * View Models used by Spring MVC REST controllers.
 */
package com.crtv.jtf.web.rest.vm;
