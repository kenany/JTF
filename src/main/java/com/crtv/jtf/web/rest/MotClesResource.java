package com.crtv.jtf.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.crtv.jtf.domain.MotCles;

import com.crtv.jtf.repository.MotClesRepository;
import com.crtv.jtf.repository.search.MotClesSearchRepository;
import com.crtv.jtf.web.rest.errors.BadRequestAlertException;
import com.crtv.jtf.web.rest.util.HeaderUtil;
import com.crtv.jtf.service.dto.MotClesDTO;
import com.crtv.jtf.service.mapper.MotClesMapper;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing MotCles.
 */
@RestController
@RequestMapping("/api")
public class MotClesResource {

    private final Logger log = LoggerFactory.getLogger(MotClesResource.class);

    private static final String ENTITY_NAME = "motCles";

    private final MotClesRepository motClesRepository;

    private final MotClesMapper motClesMapper;

    private final MotClesSearchRepository motClesSearchRepository;

    public MotClesResource(MotClesRepository motClesRepository, MotClesMapper motClesMapper, MotClesSearchRepository motClesSearchRepository) {
        this.motClesRepository = motClesRepository;
        this.motClesMapper = motClesMapper;
        this.motClesSearchRepository = motClesSearchRepository;
    }

    /**
     * POST  /mot-cles : Create a new motCles.
     *
     * @param motClesDTO the motClesDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new motClesDTO, or with status 400 (Bad Request) if the motCles has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/mot-cles")
    @Timed
    public ResponseEntity<MotClesDTO> createMotCles(@RequestBody MotClesDTO motClesDTO) throws URISyntaxException {
        log.debug("REST request to save MotCles : {}", motClesDTO);
        if (motClesDTO.getId() != null) {
            throw new BadRequestAlertException("A new motCles cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MotCles motCles = motClesMapper.toEntity(motClesDTO);
        motCles = motClesRepository.save(motCles);
        MotClesDTO result = motClesMapper.toDto(motCles);
        motClesSearchRepository.save(motCles);
        return ResponseEntity.created(new URI("/api/mot-cles/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /mot-cles : Updates an existing motCles.
     *
     * @param motClesDTO the motClesDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated motClesDTO,
     * or with status 400 (Bad Request) if the motClesDTO is not valid,
     * or with status 500 (Internal Server Error) if the motClesDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/mot-cles")
    @Timed
    public ResponseEntity<MotClesDTO> updateMotCles(@RequestBody MotClesDTO motClesDTO) throws URISyntaxException {
        log.debug("REST request to update MotCles : {}", motClesDTO);
        if (motClesDTO.getId() == null) {
            return createMotCles(motClesDTO);
        }
        MotCles motCles = motClesMapper.toEntity(motClesDTO);
        motCles = motClesRepository.save(motCles);
        MotClesDTO result = motClesMapper.toDto(motCles);
        motClesSearchRepository.save(motCles);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, motClesDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /mot-cles : get all the motCles.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of motCles in body
     */
    @GetMapping("/mot-cles")
    @Timed
    public List<MotClesDTO> getAllMotCles() {
        log.debug("REST request to get all MotCles");
        List<MotCles> motCles = motClesRepository.findAll();
        return motClesMapper.toDto(motCles);
        }

    /**
     * GET  /mot-cles/:id : get the "id" motCles.
     *
     * @param id the id of the motClesDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the motClesDTO, or with status 404 (Not Found)
     */
    @GetMapping("/mot-cles/{id}")
    @Timed
    public ResponseEntity<MotClesDTO> getMotCles(@PathVariable Long id) {
        log.debug("REST request to get MotCles : {}", id);
        MotCles motCles = motClesRepository.findOne(id);
        MotClesDTO motClesDTO = motClesMapper.toDto(motCles);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(motClesDTO));
    }

    /**
     * DELETE  /mot-cles/:id : delete the "id" motCles.
     *
     * @param id the id of the motClesDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/mot-cles/{id}")
    @Timed
    public ResponseEntity<Void> deleteMotCles(@PathVariable Long id) {
        log.debug("REST request to delete MotCles : {}", id);
        motClesRepository.delete(id);
        motClesSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/mot-cles?query=:query : search for the motCles corresponding
     * to the query.
     *
     * @param query the query of the motCles search
     * @return the result of the search
     */
    @GetMapping("/_search/mot-cles")
    @Timed
    public List<MotClesDTO> searchMotCles(@RequestParam String query) {
        log.debug("REST request to search MotCles for query {}", query);
        return StreamSupport
            .stream(motClesSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(motClesMapper::toDto)
            .collect(Collectors.toList());
    }

}
