package com.crtv.jtf.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.crtv.jtf.domain.Journal;

import com.crtv.jtf.repository.JournalRepository;
import com.crtv.jtf.repository.search.JournalSearchRepository;
import com.crtv.jtf.web.rest.errors.BadRequestAlertException;
import com.crtv.jtf.web.rest.util.HeaderUtil;
import com.crtv.jtf.web.rest.util.PaginationUtil;
import com.crtv.jtf.service.dto.JournalDTO;
import com.crtv.jtf.service.mapper.JournalMapper;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Journal.
 */
@RestController
@RequestMapping("/api")
public class JournalResource {

    private final Logger log = LoggerFactory.getLogger(JournalResource.class);
    
    private static String OS = System.getProperty("os.name").toLowerCase();
    
    private static final String WINDOW_ENV_VARIABLE = "USERPROFILE";
    
    private static final String OTHER_ENV_VARIABLE = "HOME";

    private static final String ENTITY_NAME = "journal";

    private final JournalRepository journalRepository;

    private final JournalMapper journalMapper;

    private final JournalSearchRepository journalSearchRepository;

    public JournalResource(JournalRepository journalRepository, JournalMapper journalMapper, JournalSearchRepository journalSearchRepository) {
        this.journalRepository = journalRepository;
        this.journalMapper = journalMapper;
        this.journalSearchRepository = journalSearchRepository;
    }

    /**
     * POST  /journals : Create a new journal.
     *
     * @param journalDTO the journalDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new journalDTO, or with status 400 (Bad Request) if the journal has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/journals")
    @Timed
    public ResponseEntity<JournalDTO> createJournal(@RequestBody JournalDTO journalDTO) throws URISyntaxException {
        log.debug("REST request to save Journal : {}", journalDTO);
        if (journalDTO.getId() != null) {
            throw new BadRequestAlertException("A new journal cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Journal journal = journalMapper.toEntity(journalDTO);
        journal = journalRepository.save(journal);
        JournalDTO result = journalMapper.toDto(journal);
        journalSearchRepository.save(journal);
        return ResponseEntity.created(new URI("/api/journals/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /journals : Updates an existing journal.
     *
     * @param journalDTO the journalDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated journalDTO,
     * or with status 400 (Bad Request) if the journalDTO is not valid,
     * or with status 500 (Internal Server Error) if the journalDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/journals")
    @Timed
    public ResponseEntity<JournalDTO> updateJournal(@RequestBody JournalDTO journalDTO) throws URISyntaxException {
        log.debug("REST request to update Journal : {}", journalDTO);
        if (journalDTO.getId() == null) {
            return createJournal(journalDTO);
        }
        Journal journal = journalMapper.toEntity(journalDTO);
        journal = journalRepository.save(journal);
        JournalDTO result = journalMapper.toDto(journal);
        journalSearchRepository.save(journal);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, journalDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /journals : get all the journals.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of journals in body
     */
    @GetMapping("/journals")
    @Timed
    public ResponseEntity<List<JournalDTO>> getAllJournals(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Journals");
        Page<Journal> page = journalRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/journals");
        return new ResponseEntity<>(journalMapper.toDto(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /journals/:id : get the "id" journal.
     *
     * @param id the id of the journalDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the journalDTO, or with status 404 (Not Found)
     */
    @GetMapping("/journals/{id}")
    @Timed
    public ResponseEntity<JournalDTO> getJournal(@PathVariable Long id) {
        log.debug("REST request to get Journal : {}", id);
        Journal journal = journalRepository.findOne(id);
        JournalDTO journalDTO = journalMapper.toDto(journal);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(journalDTO));
    }

    /**
     * DELETE  /journals/:id : delete the "id" journal.
     *
     * @param id the id of the journalDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/journals/{id}")
    @Timed
    public ResponseEntity<Void> deleteJournal(@PathVariable Long id) {
        log.debug("REST request to delete Journal : {}", id);
        journalRepository.delete(id);
        journalSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
    
    @GetMapping("/journals/launch/{videoName:.+}")
    @Timed
    public void launchJournal(@PathVariable String videoName) {
        log.debug("REST request to delete Journal : {}", videoName);
        String lastDirectoryName = "jtfvideos";
        String directory = System.getProperty("user.home");
//        }
    
        String fileUrl = directory+"/"+lastDirectoryName+"/"+videoName;
                
        File file = new File(fileUrl);
                
        if(file.exists()){
        try {
            if(Desktop.isDesktopSupported()){
                
                Desktop.getDesktop().open(file);
                
            }else{
                if(OS.indexOf("win") >= 0){
                    Runtime.getRuntime().exec("start "+fileUrl);
                }else if(OS.indexOf("mac") >= 0){
                    Runtime.getRuntime().exec("open "+fileUrl);
                }else{
                    Runtime.getRuntime().exec("xdg-open "+fileUrl);
                }  
            }

        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(JournalResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
    }


    /**
     * SEARCH  /_search/journals?query=:query : search for the journal corresponding
     * to the query.
     *
     * @param query the query of the journal search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/journals")
    @Timed
    public ResponseEntity<List<JournalDTO>> searchJournals(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of Journals for query {}", query);
        Page<Journal> page = journalSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/journals");
        return new ResponseEntity<>(journalMapper.toDto(page.getContent()), headers, HttpStatus.OK);
    }

}
