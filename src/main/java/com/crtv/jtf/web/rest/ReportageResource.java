package com.crtv.jtf.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.crtv.jtf.domain.Reportage;

import com.crtv.jtf.repository.ReportageRepository;
import com.crtv.jtf.repository.search.ReportageSearchRepository;
import com.crtv.jtf.web.rest.errors.BadRequestAlertException;
import com.crtv.jtf.web.rest.util.HeaderUtil;
import com.crtv.jtf.web.rest.util.PaginationUtil;
import com.crtv.jtf.service.dto.ReportageDTO;
import com.crtv.jtf.service.mapper.ReportageMapper;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Reportage.
 */
@RestController
@RequestMapping("/api")
public class ReportageResource {

    private final Logger log = LoggerFactory.getLogger(ReportageResource.class);

    private static final String ENTITY_NAME = "reportage";

    private final ReportageRepository reportageRepository;

    private final ReportageMapper reportageMapper;

    private final ReportageSearchRepository reportageSearchRepository;

    public ReportageResource(ReportageRepository reportageRepository, ReportageMapper reportageMapper, ReportageSearchRepository reportageSearchRepository) {
        this.reportageRepository = reportageRepository;
        this.reportageMapper = reportageMapper;
        this.reportageSearchRepository = reportageSearchRepository;
    }

    /**
     * POST  /reportages : Create a new reportage.
     *
     * @param reportageDTO the reportageDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new reportageDTO, or with status 400 (Bad Request) if the reportage has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/reportages")
    @Timed
    public ResponseEntity<ReportageDTO> createReportage(@RequestBody ReportageDTO reportageDTO) throws URISyntaxException {
        log.debug("REST request to save Reportage : {}", reportageDTO);
        if (reportageDTO.getId() != null) {
            throw new BadRequestAlertException("A new reportage cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Reportage reportage = reportageMapper.toEntity(reportageDTO);
        reportage = reportageRepository.save(reportage);
        ReportageDTO result = reportageMapper.toDto(reportage);
        reportageSearchRepository.save(reportage);
        return ResponseEntity.created(new URI("/api/reportages/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /reportages : Updates an existing reportage.
     *
     * @param reportageDTO the reportageDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated reportageDTO,
     * or with status 400 (Bad Request) if the reportageDTO is not valid,
     * or with status 500 (Internal Server Error) if the reportageDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/reportages")
    @Timed
    public ResponseEntity<ReportageDTO> updateReportage(@RequestBody ReportageDTO reportageDTO) throws URISyntaxException {
        log.debug("REST request to update Reportage : {}", reportageDTO);
        if (reportageDTO.getId() == null) {
            return createReportage(reportageDTO);
        }
        Reportage reportage = reportageMapper.toEntity(reportageDTO);
        reportage = reportageRepository.save(reportage);
        ReportageDTO result = reportageMapper.toDto(reportage);
        reportageSearchRepository.save(reportage);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, reportageDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /reportages : get all the reportages.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of reportages in body
     */
    @GetMapping("/reportages")
    @Timed
    public ResponseEntity<List<ReportageDTO>> getAllReportages(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Reportages");
        Page<Reportage> page = reportageRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/reportages");
        return new ResponseEntity<>(reportageMapper.toDto(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /reportages/:id : get the "id" reportage.
     *
     * @param id the id of the reportageDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the reportageDTO, or with status 404 (Not Found)
     */
    @GetMapping("/reportages/{id}")
    @Timed
    public ResponseEntity<ReportageDTO> getReportage(@PathVariable Long id) {
        log.debug("REST request to get Reportage : {}", id);
        Reportage reportage = reportageRepository.findOne(id);
        ReportageDTO reportageDTO = reportageMapper.toDto(reportage);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(reportageDTO));
    }

    /**
     * DELETE  /reportages/:id : delete the "id" reportage.
     *
     * @param id the id of the reportageDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/reportages/{id}")
    @Timed
    public ResponseEntity<Void> deleteReportage(@PathVariable Long id) {
        log.debug("REST request to delete Reportage : {}", id);
        reportageRepository.delete(id);
        reportageSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/reportages?query=:query : search for the reportage corresponding
     * to the query.
     *
     * @param query the query of the reportage search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/reportages")
    @Timed
    public ResponseEntity<List<ReportageDTO>> searchReportages(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of Reportages for query {}", query);
        Page<Reportage> page = reportageSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/reportages");
        return new ResponseEntity<>(reportageMapper.toDto(page.getContent()), headers, HttpStatus.OK);
    }

}
