(function() {
    'use strict';

    angular
        .module('jtfApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
