(function() {
    'use strict';

    angular
        .module('jtfApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('reportage-jtf', {
            parent: 'entity',
            url: '/reportage-jtf',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'jtfApp.reportage.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/reportage/reportagesjtf.html',
                    controller: 'ReportageJtfController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('reportage');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('reportage-jtf-detail', {
            parent: 'reportage-jtf',
            url: '/reportage-jtf/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'jtfApp.reportage.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/reportage/reportage-jtf-detail.html',
                    controller: 'ReportageJtfDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('reportage');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Reportage', function($stateParams, Reportage) {
                    return Reportage.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'reportage-jtf',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('reportage-jtf-detail.edit', {
            parent: 'reportage-jtf-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/reportage/reportage-jtf-dialog.html',
                    controller: 'ReportageJtfDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Reportage', function(Reportage) {
                            return Reportage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('reportage-jtf.new', {
            parent: 'reportage-jtf',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/reportage/reportage-jtf-dialog.html',
                    controller: 'ReportageJtfDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                titre: null,
                                timeCode: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('reportage-jtf', null, { reload: 'reportage-jtf' });
                }, function() {
                    $state.go('reportage-jtf');
                });
            }]
        })
        .state('reportage-jtf.edit', {
            parent: 'reportage-jtf',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/reportage/reportage-jtf-dialog.html',
                    controller: 'ReportageJtfDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Reportage', function(Reportage) {
                            return Reportage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('reportage-jtf', null, { reload: 'reportage-jtf' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('reportage-jtf.delete', {
            parent: 'reportage-jtf',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/reportage/reportage-jtf-delete-dialog.html',
                    controller: 'ReportageJtfDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Reportage', function(Reportage) {
                            return Reportage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('reportage-jtf', null, { reload: 'reportage-jtf' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
