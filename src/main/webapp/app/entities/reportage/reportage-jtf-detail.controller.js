(function() {
    'use strict';

    angular
        .module('jtfApp')
        .controller('ReportageJtfDetailController', ReportageJtfDetailController);

    ReportageJtfDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Reportage', 'Journal', 'Person', 'MotCles'];

    function ReportageJtfDetailController($scope, $rootScope, $stateParams, previousState, entity, Reportage, Journal, Person, MotCles) {
        var vm = this;

        vm.reportage = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('jtfApp:reportageUpdate', function(event, result) {
            vm.reportage = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
