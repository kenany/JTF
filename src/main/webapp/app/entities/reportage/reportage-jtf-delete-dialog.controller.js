(function() {
    'use strict';

    angular
        .module('jtfApp')
        .controller('ReportageJtfDeleteController',ReportageJtfDeleteController);

    ReportageJtfDeleteController.$inject = ['$uibModalInstance', 'entity', 'Reportage'];

    function ReportageJtfDeleteController($uibModalInstance, entity, Reportage) {
        var vm = this;

        vm.reportage = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Reportage.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
