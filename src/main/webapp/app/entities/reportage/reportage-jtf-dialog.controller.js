(function() {
    'use strict';

    angular
        .module('jtfApp')
        .controller('ReportageJtfDialogController', ReportageJtfDialogController);

    ReportageJtfDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Reportage', 'Journal', 'Person', 'MotCles'];

    function ReportageJtfDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Reportage, Journal, Person, MotCles) {
        var vm = this;

        vm.reportage = entity;
        vm.clear = clear;
        vm.save = save;
        vm.journals = Journal.query();
        vm.people = Person.query();
        vm.motcles = MotCles.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.reportage.id !== null) {
                Reportage.update(vm.reportage, onSaveSuccess, onSaveError);
            } else {
                Reportage.save(vm.reportage, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('jtfApp:reportageUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
