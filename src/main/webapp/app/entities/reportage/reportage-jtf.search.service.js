(function() {
    'use strict';

    angular
        .module('jtfApp')
        .factory('ReportageSearch', ReportageSearch);

    ReportageSearch.$inject = ['$resource'];

    function ReportageSearch($resource) {
        var resourceUrl =  'api/_search/reportages/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
