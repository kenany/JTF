(function() {
    'use strict';
    angular
        .module('jtfApp')
        .factory('Reportage', Reportage);

    Reportage.$inject = ['$resource'];

    function Reportage ($resource) {
        var resourceUrl =  'api/reportages/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
