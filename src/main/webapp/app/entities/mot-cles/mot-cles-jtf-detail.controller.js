(function() {
    'use strict';

    angular
        .module('jtfApp')
        .controller('MotClesJtfDetailController', MotClesJtfDetailController);

    MotClesJtfDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'MotCles', 'Reportage'];

    function MotClesJtfDetailController($scope, $rootScope, $stateParams, previousState, entity, MotCles, Reportage) {
        var vm = this;

        vm.motCles = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('jtfApp:motClesUpdate', function(event, result) {
            vm.motCles = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
