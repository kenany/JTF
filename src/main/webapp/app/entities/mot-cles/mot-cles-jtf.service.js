(function() {
    'use strict';
    angular
        .module('jtfApp')
        .factory('MotCles', MotCles);

    MotCles.$inject = ['$resource'];

    function MotCles ($resource) {
        var resourceUrl =  'api/mot-cles/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
