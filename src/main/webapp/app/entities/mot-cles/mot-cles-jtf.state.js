(function() {
    'use strict';

    angular
        .module('jtfApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('mot-cles-jtf', {
            parent: 'entity',
            url: '/mot-cles-jtf',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'jtfApp.motCles.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/mot-cles/mot-clesjtf.html',
                    controller: 'MotClesJtfController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('motCles');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('mot-cles-jtf-detail', {
            parent: 'mot-cles-jtf',
            url: '/mot-cles-jtf/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'jtfApp.motCles.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/mot-cles/mot-cles-jtf-detail.html',
                    controller: 'MotClesJtfDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('motCles');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'MotCles', function($stateParams, MotCles) {
                    return MotCles.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'mot-cles-jtf',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('mot-cles-jtf-detail.edit', {
            parent: 'mot-cles-jtf-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/mot-cles/mot-cles-jtf-dialog.html',
                    controller: 'MotClesJtfDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['MotCles', function(MotCles) {
                            return MotCles.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('mot-cles-jtf.new', {
            parent: 'mot-cles-jtf',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/mot-cles/mot-cles-jtf-dialog.html',
                    controller: 'MotClesJtfDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                mot: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('mot-cles-jtf', null, { reload: 'mot-cles-jtf' });
                }, function() {
                    $state.go('mot-cles-jtf');
                });
            }]
        })
        .state('mot-cles-jtf.edit', {
            parent: 'mot-cles-jtf',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/mot-cles/mot-cles-jtf-dialog.html',
                    controller: 'MotClesJtfDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['MotCles', function(MotCles) {
                            return MotCles.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('mot-cles-jtf', null, { reload: 'mot-cles-jtf' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('mot-cles-jtf.delete', {
            parent: 'mot-cles-jtf',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/mot-cles/mot-cles-jtf-delete-dialog.html',
                    controller: 'MotClesJtfDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['MotCles', function(MotCles) {
                            return MotCles.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('mot-cles-jtf', null, { reload: 'mot-cles-jtf' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
