(function() {
    'use strict';

    angular
        .module('jtfApp')
        .controller('MotClesJtfDialogController', MotClesJtfDialogController);

    MotClesJtfDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'MotCles', 'Reportage'];

    function MotClesJtfDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, MotCles, Reportage) {
        var vm = this;

        vm.motCles = entity;
        vm.clear = clear;
        vm.save = save;
        vm.reportages = Reportage.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.motCles.id !== null) {
                MotCles.update(vm.motCles, onSaveSuccess, onSaveError);
            } else {
                MotCles.save(vm.motCles, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('jtfApp:motClesUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
