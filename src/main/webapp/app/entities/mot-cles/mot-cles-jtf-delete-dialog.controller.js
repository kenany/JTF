(function() {
    'use strict';

    angular
        .module('jtfApp')
        .controller('MotClesJtfDeleteController',MotClesJtfDeleteController);

    MotClesJtfDeleteController.$inject = ['$uibModalInstance', 'entity', 'MotCles'];

    function MotClesJtfDeleteController($uibModalInstance, entity, MotCles) {
        var vm = this;

        vm.motCles = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            MotCles.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
