(function() {
    'use strict';

    angular
        .module('jtfApp')
        .factory('MotClesSearch', MotClesSearch);

    MotClesSearch.$inject = ['$resource'];

    function MotClesSearch($resource) {
        var resourceUrl =  'api/_search/mot-cles/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
