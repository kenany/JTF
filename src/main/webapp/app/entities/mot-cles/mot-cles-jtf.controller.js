(function() {
    'use strict';

    angular
        .module('jtfApp')
        .controller('MotClesJtfController', MotClesJtfController);

    MotClesJtfController.$inject = ['MotCles', 'MotClesSearch'];

    function MotClesJtfController(MotCles, MotClesSearch) {

        var vm = this;

        vm.motCles = [];
        vm.clear = clear;
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            MotCles.query(function(result) {
                vm.motCles = result;
                vm.searchQuery = null;
            });
        }

        function search() {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            MotClesSearch.query({query: vm.searchQuery}, function(result) {
                vm.motCles = result;
                vm.currentSearch = vm.searchQuery;
            });
        }

        function clear() {
            vm.searchQuery = null;
            loadAll();
        }    }
})();
