(function() {
    'use strict';

    angular
        .module('jtfApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('role-jtf', {
            parent: 'entity',
            url: '/role-jtf',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'jtfApp.role.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/role/rolesjtf.html',
                    controller: 'RoleJtfController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('role');
                    $translatePartialLoader.addPart('rolesNames');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('role-jtf-detail', {
            parent: 'role-jtf',
            url: '/role-jtf/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'jtfApp.role.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/role/role-jtf-detail.html',
                    controller: 'RoleJtfDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('role');
                    $translatePartialLoader.addPart('rolesNames');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Role', function($stateParams, Role) {
                    return Role.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'role-jtf',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('role-jtf-detail.edit', {
            parent: 'role-jtf-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/role/role-jtf-dialog.html',
                    controller: 'RoleJtfDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Role', function(Role) {
                            return Role.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('role-jtf.new', {
            parent: 'role-jtf',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/role/role-jtf-dialog.html',
                    controller: 'RoleJtfDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('role-jtf', null, { reload: 'role-jtf' });
                }, function() {
                    $state.go('role-jtf');
                });
            }]
        })
        .state('role-jtf.edit', {
            parent: 'role-jtf',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/role/role-jtf-dialog.html',
                    controller: 'RoleJtfDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Role', function(Role) {
                            return Role.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('role-jtf', null, { reload: 'role-jtf' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('role-jtf.delete', {
            parent: 'role-jtf',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/role/role-jtf-delete-dialog.html',
                    controller: 'RoleJtfDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Role', function(Role) {
                            return Role.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('role-jtf', null, { reload: 'role-jtf' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
