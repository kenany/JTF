(function() {
    'use strict';

    angular
        .module('jtfApp')
        .controller('RoleJtfDetailController', RoleJtfDetailController);

    RoleJtfDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Role', 'Person'];

    function RoleJtfDetailController($scope, $rootScope, $stateParams, previousState, entity, Role, Person) {
        var vm = this;

        vm.role = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('jtfApp:roleUpdate', function(event, result) {
            vm.role = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
