(function() {
    'use strict';

    angular
        .module('jtfApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('journal', {
            parent: 'entity',
            url: '/journal?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'jtfApp.journal.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/journal/journalsjtf.html',
                    controller: 'JournalJtfController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('journal');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('journal-detail', {
            parent: 'journal',
            url: '/journal/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'jtfApp.journal.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/journal/journal-jtf-detail.html',
                    controller: 'JournalJtfDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('journal');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Journal', function($stateParams, Journal) {
                    return Journal.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'journal-jtf',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('journal-detail.edit', {
            parent: 'journal-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/journal/journal-jtf-dialog.html',
                    controller: 'JournalJtfDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Journal', function(Journal) {
                            return Journal.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('journal.new', {
            parent: 'journal',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/journal/journal-jtf-dialog.html',
                    controller: 'JournalJtfDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                cote: null,
                                date: null,
                                duree: null,
                                lieu: null,
                                qualite: null,
                                videoUrl: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('journal', null, { reload: 'journal' });
                }, function() {
                    $state.go('journal');
                });
            }]
        })
        .state('journal.edit', {
            parent: 'journal',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/journal/journal-jtf-dialog.html',
                    controller: 'JournalJtfDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Journal', function(Journal) {
                            return Journal.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('journal', null, { reload: 'journal' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('journal.delete', {
            parent: 'journal',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/journal/journal-jtf-delete-dialog.html',
                    controller: 'JournalJtfDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Journal', function(Journal) {
                            return Journal.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('journal', null, { reload: 'journal' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
