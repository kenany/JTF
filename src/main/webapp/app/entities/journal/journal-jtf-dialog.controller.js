(function() {
    'use strict';

    angular
        .module('jtfApp')
        .controller('JournalJtfDialogController', JournalJtfDialogController);

    JournalJtfDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Journal', 'Reportage', 'Person'];

    function JournalJtfDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Journal, Reportage, Person) {
        var vm = this;

        vm.journal = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.reportages = Reportage.query();
        vm.people = Person.query();
        vm.setFileName = setFileName;
        

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.journal.id !== null) {
                Journal.update(vm.journal, onSaveSuccess, onSaveError);
            } else {
                Journal.save(vm.journal, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('jtfApp:journalUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.date = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }

        function setFileName(fileInput){

            if(fileInput !== null){
            console.info(fileInput);
            $('#fileName').val(fileInput.name);
            vm.journal.videoUrl = fileInput.name;
        }

        }
    }
})();
