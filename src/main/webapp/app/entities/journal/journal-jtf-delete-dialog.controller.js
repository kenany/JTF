(function() {
    'use strict';

    angular
        .module('jtfApp')
        .controller('JournalJtfDeleteController',JournalJtfDeleteController);

    JournalJtfDeleteController.$inject = ['$uibModalInstance', 'entity', 'Journal'];

    function JournalJtfDeleteController($uibModalInstance, entity, Journal) {
        var vm = this;

        vm.journal = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Journal.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
