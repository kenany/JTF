(function() {
    'use strict';

    angular
        .module('jtfApp')
        .controller('JournalJtfDetailController', JournalJtfDetailController);

    JournalJtfDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Journal', 'Reportage', 'Person'];

    function JournalJtfDetailController($scope, $rootScope, $stateParams, previousState, entity, Journal, Reportage, Person) {
        var vm = this;

        vm.journal = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('jtfApp:journalUpdate', function(event, result) {
            vm.journal = result;
            setFileName();
        });
        $scope.$on('$destroy', unsubscribe);

        function setFileName(){
            
                        // if(fileInput !== null){
                        // console.info(fileInput);
                        $('#fileName').val(vm.journal.videoUrl);
                        // vm.journal.videoUrl = fileInput.name;
                    // }
                }
    }
})();
