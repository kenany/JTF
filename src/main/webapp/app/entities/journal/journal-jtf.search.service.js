(function() {
    'use strict';

    angular
        .module('jtfApp')
        .factory('JournalSearch', JournalSearch);

    JournalSearch.$inject = ['$resource'];

    function JournalSearch($resource) {
        var resourceUrl =  'api/_search/journals/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
