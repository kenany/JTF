(function() {
    'use strict';

    angular
        .module('jtfApp')
        .controller('JournalJtfController', JournalJtfController);

    JournalJtfController.$inject = ['Journal', 
                                    'JournalSearch', 
                                    'ParseLinks', 
                                    'AlertService', 
                                    'paginationConstants',
                                    'pagingParams',
                                    'Upload'];

    function JournalJtfController(Journal, 
                                  JournalSearch, 
                                  ParseLinks, 
                                  AlertService, 
                                  paginationConstants,
                                  pagingParams,
                                  Upload) {

        var vm = this;

        vm.journals = [];
        vm.loadPage = loadPage;
        vm.itemsPerPage = paginationConstants.itemsPerPage;
        vm.page = 0;
        vm.links = {
            last: 0
        };
        // vm.predicate = 'id';
        vm.predicate = pagingParams.predicate;
        vm.reset = reset;
        // vm.reverse = true;
        vm.reverse = pagingParams.ascending;
        vm.transition = transition;
        vm.searchQuery = pagingParams.search;
        vm.currentSearch = pagingParams.search;
        vm.clear = clear;
        vm.loadAll = loadAll;
        vm.search = search;
        vm.launchJournal = launchJournal

        loadAll();

        function loadAll () {
            if (vm.currentSearch) {
                JournalSearch.query({
                    query: vm.currentSearch,
                    page: vm.page,
                    size: vm.itemsPerPage,
                    sort: sort()
                }, onSuccess, onError);
            } else {
                Journal.query({
                    page: vm.page,
                    size: vm.itemsPerPage,
                    sort: sort()
                }, onSuccess, onError);
            }
            function sort() {
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'id') {
                    result.push('id');
                }
                return result;
            }

            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                for (var i = 0; i < data.length; i++) {
                    vm.journals.push(data[i]);
                }
            }

            function onError(error) {
                AlertService.error(error.data.message);
            }
        }

        function reset () {
            vm.page = 0;
            vm.journals = [];
            loadAll();
        }

        function loadPage(page) {
            vm.page = page;
            loadAll();
        }

        function clear () {
            vm.journals = [];
            vm.links = {
                last: 0
            };
            vm.page = 0;
            vm.predicate = 'id';
            vm.reverse = true;
            vm.searchQuery = null;
            vm.currentSearch = null;
            vm.loadAll();
        }

        function search (searchQuery) {
            if (!searchQuery){
                return vm.clear();
            }
            vm.journals = [];
            vm.links = {
                last: 0
            };
            vm.page = 0;
            vm.predicate = '_score';
            vm.reverse = false;
            vm.currentSearch = searchQuery;
            vm.loadAll();
        }

        function transition() {
            $state.transitionTo($state.$current, {
                page: vm.page,
                sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
                search: vm.currentSearch
            });
        }

        function launchJournal(event, name){
            event.preventDefault();
            console.info(name);
            Journal.get({id: 'launch', name: name});
        }
    }
})();
