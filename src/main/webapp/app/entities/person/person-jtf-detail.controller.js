(function() {
    'use strict';

    angular
        .module('jtfApp')
        .controller('PersonJtfDetailController', PersonJtfDetailController);

    PersonJtfDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Person', 'Journal', 'Reportage', 'Role'];

    function PersonJtfDetailController($scope, $rootScope, $stateParams, previousState, entity, Person, Journal, Reportage, Role) {
        var vm = this;

        vm.person = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('jtfApp:personUpdate', function(event, result) {
            vm.person = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
