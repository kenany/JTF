(function() {
    'use strict';

    angular
        .module('jtfApp')
        .controller('PersonJtfDialogController', PersonJtfDialogController);

    PersonJtfDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Person', 'Journal', 'Reportage', 'Role'];

    function PersonJtfDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Person, Journal, Reportage, Role) {
        var vm = this;

        vm.person = entity;
        vm.clear = clear;
        vm.save = save;
        vm.journals = Journal.query();
        vm.reportages = Reportage.query();
        vm.roles = Role.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.person.id !== null) {
                Person.update(vm.person, onSaveSuccess, onSaveError);
            } else {
                Person.save(vm.person, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('jtfApp:personUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
