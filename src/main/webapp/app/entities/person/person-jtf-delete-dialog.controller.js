(function() {
    'use strict';

    angular
        .module('jtfApp')
        .controller('PersonJtfDeleteController',PersonJtfDeleteController);

    PersonJtfDeleteController.$inject = ['$uibModalInstance', 'entity', 'Person'];

    function PersonJtfDeleteController($uibModalInstance, entity, Person) {
        var vm = this;

        vm.person = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Person.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
