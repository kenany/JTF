package com.crtv.jtf.web.rest;

import com.crtv.jtf.JtfApp;

import com.crtv.jtf.domain.MotCles;
import com.crtv.jtf.repository.MotClesRepository;
import com.crtv.jtf.repository.search.MotClesSearchRepository;
import com.crtv.jtf.service.dto.MotClesDTO;
import com.crtv.jtf.service.mapper.MotClesMapper;
import com.crtv.jtf.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MotClesResource REST controller.
 *
 * @see MotClesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JtfApp.class)
public class MotClesResourceIntTest {

    private static final String DEFAULT_MOT = "AAAAAAAAAA";
    private static final String UPDATED_MOT = "BBBBBBBBBB";

    @Autowired
    private MotClesRepository motClesRepository;

    @Autowired
    private MotClesMapper motClesMapper;

    @Autowired
    private MotClesSearchRepository motClesSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restMotClesMockMvc;

    private MotCles motCles;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MotClesResource motClesResource = new MotClesResource(motClesRepository, motClesMapper, motClesSearchRepository);
        this.restMotClesMockMvc = MockMvcBuilders.standaloneSetup(motClesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MotCles createEntity(EntityManager em) {
        MotCles motCles = new MotCles()
            .mot(DEFAULT_MOT);
        return motCles;
    }

    @Before
    public void initTest() {
        motClesSearchRepository.deleteAll();
        motCles = createEntity(em);
    }

    @Test
    @Transactional
    public void createMotCles() throws Exception {
        int databaseSizeBeforeCreate = motClesRepository.findAll().size();

        // Create the MotCles
        MotClesDTO motClesDTO = motClesMapper.toDto(motCles);
        restMotClesMockMvc.perform(post("/api/mot-cles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(motClesDTO)))
            .andExpect(status().isCreated());

        // Validate the MotCles in the database
        List<MotCles> motClesList = motClesRepository.findAll();
        assertThat(motClesList).hasSize(databaseSizeBeforeCreate + 1);
        MotCles testMotCles = motClesList.get(motClesList.size() - 1);
        assertThat(testMotCles.getMot()).isEqualTo(DEFAULT_MOT);

        // Validate the MotCles in Elasticsearch
        MotCles motClesEs = motClesSearchRepository.findOne(testMotCles.getId());
        assertThat(motClesEs).isEqualToComparingFieldByField(testMotCles);
    }

    @Test
    @Transactional
    public void createMotClesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = motClesRepository.findAll().size();

        // Create the MotCles with an existing ID
        motCles.setId(1L);
        MotClesDTO motClesDTO = motClesMapper.toDto(motCles);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMotClesMockMvc.perform(post("/api/mot-cles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(motClesDTO)))
            .andExpect(status().isBadRequest());

        // Validate the MotCles in the database
        List<MotCles> motClesList = motClesRepository.findAll();
        assertThat(motClesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllMotCles() throws Exception {
        // Initialize the database
        motClesRepository.saveAndFlush(motCles);

        // Get all the motClesList
        restMotClesMockMvc.perform(get("/api/mot-cles?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(motCles.getId().intValue())))
            .andExpect(jsonPath("$.[*].mot").value(hasItem(DEFAULT_MOT.toString())));
    }

    @Test
    @Transactional
    public void getMotCles() throws Exception {
        // Initialize the database
        motClesRepository.saveAndFlush(motCles);

        // Get the motCles
        restMotClesMockMvc.perform(get("/api/mot-cles/{id}", motCles.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(motCles.getId().intValue()))
            .andExpect(jsonPath("$.mot").value(DEFAULT_MOT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingMotCles() throws Exception {
        // Get the motCles
        restMotClesMockMvc.perform(get("/api/mot-cles/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMotCles() throws Exception {
        // Initialize the database
        motClesRepository.saveAndFlush(motCles);
        motClesSearchRepository.save(motCles);
        int databaseSizeBeforeUpdate = motClesRepository.findAll().size();

        // Update the motCles
        MotCles updatedMotCles = motClesRepository.findOne(motCles.getId());
        updatedMotCles
            .mot(UPDATED_MOT);
        MotClesDTO motClesDTO = motClesMapper.toDto(updatedMotCles);

        restMotClesMockMvc.perform(put("/api/mot-cles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(motClesDTO)))
            .andExpect(status().isOk());

        // Validate the MotCles in the database
        List<MotCles> motClesList = motClesRepository.findAll();
        assertThat(motClesList).hasSize(databaseSizeBeforeUpdate);
        MotCles testMotCles = motClesList.get(motClesList.size() - 1);
        assertThat(testMotCles.getMot()).isEqualTo(UPDATED_MOT);

        // Validate the MotCles in Elasticsearch
        MotCles motClesEs = motClesSearchRepository.findOne(testMotCles.getId());
        assertThat(motClesEs).isEqualToComparingFieldByField(testMotCles);
    }

    @Test
    @Transactional
    public void updateNonExistingMotCles() throws Exception {
        int databaseSizeBeforeUpdate = motClesRepository.findAll().size();

        // Create the MotCles
        MotClesDTO motClesDTO = motClesMapper.toDto(motCles);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restMotClesMockMvc.perform(put("/api/mot-cles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(motClesDTO)))
            .andExpect(status().isCreated());

        // Validate the MotCles in the database
        List<MotCles> motClesList = motClesRepository.findAll();
        assertThat(motClesList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteMotCles() throws Exception {
        // Initialize the database
        motClesRepository.saveAndFlush(motCles);
        motClesSearchRepository.save(motCles);
        int databaseSizeBeforeDelete = motClesRepository.findAll().size();

        // Get the motCles
        restMotClesMockMvc.perform(delete("/api/mot-cles/{id}", motCles.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean motClesExistsInEs = motClesSearchRepository.exists(motCles.getId());
        assertThat(motClesExistsInEs).isFalse();

        // Validate the database is empty
        List<MotCles> motClesList = motClesRepository.findAll();
        assertThat(motClesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchMotCles() throws Exception {
        // Initialize the database
        motClesRepository.saveAndFlush(motCles);
        motClesSearchRepository.save(motCles);

        // Search the motCles
        restMotClesMockMvc.perform(get("/api/_search/mot-cles?query=id:" + motCles.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(motCles.getId().intValue())))
            .andExpect(jsonPath("$.[*].mot").value(hasItem(DEFAULT_MOT.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MotCles.class);
        MotCles motCles1 = new MotCles();
        motCles1.setId(1L);
        MotCles motCles2 = new MotCles();
        motCles2.setId(motCles1.getId());
        assertThat(motCles1).isEqualTo(motCles2);
        motCles2.setId(2L);
        assertThat(motCles1).isNotEqualTo(motCles2);
        motCles1.setId(null);
        assertThat(motCles1).isNotEqualTo(motCles2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(MotClesDTO.class);
        MotClesDTO motClesDTO1 = new MotClesDTO();
        motClesDTO1.setId(1L);
        MotClesDTO motClesDTO2 = new MotClesDTO();
        assertThat(motClesDTO1).isNotEqualTo(motClesDTO2);
        motClesDTO2.setId(motClesDTO1.getId());
        assertThat(motClesDTO1).isEqualTo(motClesDTO2);
        motClesDTO2.setId(2L);
        assertThat(motClesDTO1).isNotEqualTo(motClesDTO2);
        motClesDTO1.setId(null);
        assertThat(motClesDTO1).isNotEqualTo(motClesDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(motClesMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(motClesMapper.fromId(null)).isNull();
    }
}
