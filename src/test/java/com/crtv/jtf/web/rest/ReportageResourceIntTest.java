package com.crtv.jtf.web.rest;

import com.crtv.jtf.JtfApp;

import com.crtv.jtf.domain.Reportage;
import com.crtv.jtf.repository.ReportageRepository;
import com.crtv.jtf.repository.search.ReportageSearchRepository;
import com.crtv.jtf.service.dto.ReportageDTO;
import com.crtv.jtf.service.mapper.ReportageMapper;
import com.crtv.jtf.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ReportageResource REST controller.
 *
 * @see ReportageResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JtfApp.class)
public class ReportageResourceIntTest {

    private static final String DEFAULT_TITRE = "AAAAAAAAAA";
    private static final String UPDATED_TITRE = "BBBBBBBBBB";

    private static final String DEFAULT_TIME_CODE = "AAAAAAAAAA";
    private static final String UPDATED_TIME_CODE = "BBBBBBBBBB";

    @Autowired
    private ReportageRepository reportageRepository;

    @Autowired
    private ReportageMapper reportageMapper;

    @Autowired
    private ReportageSearchRepository reportageSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restReportageMockMvc;

    private Reportage reportage;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ReportageResource reportageResource = new ReportageResource(reportageRepository, reportageMapper, reportageSearchRepository);
        this.restReportageMockMvc = MockMvcBuilders.standaloneSetup(reportageResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Reportage createEntity(EntityManager em) {
        Reportage reportage = new Reportage()
            .titre(DEFAULT_TITRE)
            .timeCode(DEFAULT_TIME_CODE);
        return reportage;
    }

    @Before
    public void initTest() {
        reportageSearchRepository.deleteAll();
        reportage = createEntity(em);
    }

    @Test
    @Transactional
    public void createReportage() throws Exception {
        int databaseSizeBeforeCreate = reportageRepository.findAll().size();

        // Create the Reportage
        ReportageDTO reportageDTO = reportageMapper.toDto(reportage);
        restReportageMockMvc.perform(post("/api/reportages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(reportageDTO)))
            .andExpect(status().isCreated());

        // Validate the Reportage in the database
        List<Reportage> reportageList = reportageRepository.findAll();
        assertThat(reportageList).hasSize(databaseSizeBeforeCreate + 1);
        Reportage testReportage = reportageList.get(reportageList.size() - 1);
        assertThat(testReportage.getTitre()).isEqualTo(DEFAULT_TITRE);
        assertThat(testReportage.getTimeCode()).isEqualTo(DEFAULT_TIME_CODE);

        // Validate the Reportage in Elasticsearch
        Reportage reportageEs = reportageSearchRepository.findOne(testReportage.getId());
        assertThat(reportageEs).isEqualToComparingFieldByField(testReportage);
    }

    @Test
    @Transactional
    public void createReportageWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = reportageRepository.findAll().size();

        // Create the Reportage with an existing ID
        reportage.setId(1L);
        ReportageDTO reportageDTO = reportageMapper.toDto(reportage);

        // An entity with an existing ID cannot be created, so this API call must fail
        restReportageMockMvc.perform(post("/api/reportages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(reportageDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Reportage in the database
        List<Reportage> reportageList = reportageRepository.findAll();
        assertThat(reportageList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllReportages() throws Exception {
        // Initialize the database
        reportageRepository.saveAndFlush(reportage);

        // Get all the reportageList
        restReportageMockMvc.perform(get("/api/reportages?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(reportage.getId().intValue())))
            .andExpect(jsonPath("$.[*].titre").value(hasItem(DEFAULT_TITRE.toString())))
            .andExpect(jsonPath("$.[*].timeCode").value(hasItem(DEFAULT_TIME_CODE.toString())));
    }

    @Test
    @Transactional
    public void getReportage() throws Exception {
        // Initialize the database
        reportageRepository.saveAndFlush(reportage);

        // Get the reportage
        restReportageMockMvc.perform(get("/api/reportages/{id}", reportage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(reportage.getId().intValue()))
            .andExpect(jsonPath("$.titre").value(DEFAULT_TITRE.toString()))
            .andExpect(jsonPath("$.timeCode").value(DEFAULT_TIME_CODE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingReportage() throws Exception {
        // Get the reportage
        restReportageMockMvc.perform(get("/api/reportages/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateReportage() throws Exception {
        // Initialize the database
        reportageRepository.saveAndFlush(reportage);
        reportageSearchRepository.save(reportage);
        int databaseSizeBeforeUpdate = reportageRepository.findAll().size();

        // Update the reportage
        Reportage updatedReportage = reportageRepository.findOne(reportage.getId());
        updatedReportage
            .titre(UPDATED_TITRE)
            .timeCode(UPDATED_TIME_CODE);
        ReportageDTO reportageDTO = reportageMapper.toDto(updatedReportage);

        restReportageMockMvc.perform(put("/api/reportages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(reportageDTO)))
            .andExpect(status().isOk());

        // Validate the Reportage in the database
        List<Reportage> reportageList = reportageRepository.findAll();
        assertThat(reportageList).hasSize(databaseSizeBeforeUpdate);
        Reportage testReportage = reportageList.get(reportageList.size() - 1);
        assertThat(testReportage.getTitre()).isEqualTo(UPDATED_TITRE);
        assertThat(testReportage.getTimeCode()).isEqualTo(UPDATED_TIME_CODE);

        // Validate the Reportage in Elasticsearch
        Reportage reportageEs = reportageSearchRepository.findOne(testReportage.getId());
        assertThat(reportageEs).isEqualToComparingFieldByField(testReportage);
    }

    @Test
    @Transactional
    public void updateNonExistingReportage() throws Exception {
        int databaseSizeBeforeUpdate = reportageRepository.findAll().size();

        // Create the Reportage
        ReportageDTO reportageDTO = reportageMapper.toDto(reportage);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restReportageMockMvc.perform(put("/api/reportages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(reportageDTO)))
            .andExpect(status().isCreated());

        // Validate the Reportage in the database
        List<Reportage> reportageList = reportageRepository.findAll();
        assertThat(reportageList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteReportage() throws Exception {
        // Initialize the database
        reportageRepository.saveAndFlush(reportage);
        reportageSearchRepository.save(reportage);
        int databaseSizeBeforeDelete = reportageRepository.findAll().size();

        // Get the reportage
        restReportageMockMvc.perform(delete("/api/reportages/{id}", reportage.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean reportageExistsInEs = reportageSearchRepository.exists(reportage.getId());
        assertThat(reportageExistsInEs).isFalse();

        // Validate the database is empty
        List<Reportage> reportageList = reportageRepository.findAll();
        assertThat(reportageList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchReportage() throws Exception {
        // Initialize the database
        reportageRepository.saveAndFlush(reportage);
        reportageSearchRepository.save(reportage);

        // Search the reportage
        restReportageMockMvc.perform(get("/api/_search/reportages?query=id:" + reportage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(reportage.getId().intValue())))
            .andExpect(jsonPath("$.[*].titre").value(hasItem(DEFAULT_TITRE.toString())))
            .andExpect(jsonPath("$.[*].timeCode").value(hasItem(DEFAULT_TIME_CODE.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Reportage.class);
        Reportage reportage1 = new Reportage();
        reportage1.setId(1L);
        Reportage reportage2 = new Reportage();
        reportage2.setId(reportage1.getId());
        assertThat(reportage1).isEqualTo(reportage2);
        reportage2.setId(2L);
        assertThat(reportage1).isNotEqualTo(reportage2);
        reportage1.setId(null);
        assertThat(reportage1).isNotEqualTo(reportage2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ReportageDTO.class);
        ReportageDTO reportageDTO1 = new ReportageDTO();
        reportageDTO1.setId(1L);
        ReportageDTO reportageDTO2 = new ReportageDTO();
        assertThat(reportageDTO1).isNotEqualTo(reportageDTO2);
        reportageDTO2.setId(reportageDTO1.getId());
        assertThat(reportageDTO1).isEqualTo(reportageDTO2);
        reportageDTO2.setId(2L);
        assertThat(reportageDTO1).isNotEqualTo(reportageDTO2);
        reportageDTO1.setId(null);
        assertThat(reportageDTO1).isNotEqualTo(reportageDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(reportageMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(reportageMapper.fromId(null)).isNull();
    }
}
