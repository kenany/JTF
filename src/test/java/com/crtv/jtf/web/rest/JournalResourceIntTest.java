package com.crtv.jtf.web.rest;

import com.crtv.jtf.JtfApp;

import com.crtv.jtf.domain.Journal;
import com.crtv.jtf.repository.JournalRepository;
import com.crtv.jtf.repository.search.JournalSearchRepository;
import com.crtv.jtf.service.dto.JournalDTO;
import com.crtv.jtf.service.mapper.JournalMapper;
import com.crtv.jtf.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the JournalResource REST controller.
 *
 * @see JournalResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JtfApp.class)
public class JournalResourceIntTest {

    private static final String DEFAULT_COTE = "AAAAAAAAAA";
    private static final String UPDATED_COTE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Integer DEFAULT_DUREE = 1;
    private static final Integer UPDATED_DUREE = 2;

    private static final String DEFAULT_LIEU = "AAAAAAAAAA";
    private static final String UPDATED_LIEU = "BBBBBBBBBB";

    private static final String DEFAULT_QUALITE = "AAAAAAAAAA";
    private static final String UPDATED_QUALITE = "BBBBBBBBBB";

    private static final String DEFAULT_VIDEO_URL = "AAAAAAAAAA";
    private static final String UPDATED_VIDEO_URL = "BBBBBBBBBB";

    @Autowired
    private JournalRepository journalRepository;

    @Autowired
    private JournalMapper journalMapper;

    @Autowired
    private JournalSearchRepository journalSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restJournalMockMvc;

    private Journal journal;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final JournalResource journalResource = new JournalResource(journalRepository, journalMapper, journalSearchRepository);
        this.restJournalMockMvc = MockMvcBuilders.standaloneSetup(journalResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Journal createEntity(EntityManager em) {
        Journal journal = new Journal()
            .cote(DEFAULT_COTE)
            .date(DEFAULT_DATE)
            .duree(DEFAULT_DUREE)
            .lieu(DEFAULT_LIEU)
            .qualite(DEFAULT_QUALITE)
            .videoUrl(DEFAULT_VIDEO_URL);
        return journal;
    }

    @Before
    public void initTest() {
        journalSearchRepository.deleteAll();
        journal = createEntity(em);
    }

    @Test
    @Transactional
    public void createJournal() throws Exception {
        int databaseSizeBeforeCreate = journalRepository.findAll().size();

        // Create the Journal
        JournalDTO journalDTO = journalMapper.toDto(journal);
        restJournalMockMvc.perform(post("/api/journals")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(journalDTO)))
            .andExpect(status().isCreated());

        // Validate the Journal in the database
        List<Journal> journalList = journalRepository.findAll();
        assertThat(journalList).hasSize(databaseSizeBeforeCreate + 1);
        Journal testJournal = journalList.get(journalList.size() - 1);
        assertThat(testJournal.getCote()).isEqualTo(DEFAULT_COTE);
        assertThat(testJournal.getDate()).isEqualTo(DEFAULT_DATE);
        assertThat(testJournal.getDuree()).isEqualTo(DEFAULT_DUREE);
        assertThat(testJournal.getLieu()).isEqualTo(DEFAULT_LIEU);
        assertThat(testJournal.getQualite()).isEqualTo(DEFAULT_QUALITE);
        assertThat(testJournal.getVideoUrl()).isEqualTo(DEFAULT_VIDEO_URL);

        // Validate the Journal in Elasticsearch
        Journal journalEs = journalSearchRepository.findOne(testJournal.getId());
        assertThat(journalEs).isEqualToComparingFieldByField(testJournal);
    }

    @Test
    @Transactional
    public void createJournalWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = journalRepository.findAll().size();

        // Create the Journal with an existing ID
        journal.setId(1L);
        JournalDTO journalDTO = journalMapper.toDto(journal);

        // An entity with an existing ID cannot be created, so this API call must fail
        restJournalMockMvc.perform(post("/api/journals")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(journalDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Journal in the database
        List<Journal> journalList = journalRepository.findAll();
        assertThat(journalList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllJournals() throws Exception {
        // Initialize the database
        journalRepository.saveAndFlush(journal);

        // Get all the journalList
        restJournalMockMvc.perform(get("/api/journals?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(journal.getId().intValue())))
            .andExpect(jsonPath("$.[*].cote").value(hasItem(DEFAULT_COTE.toString())))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())))
            .andExpect(jsonPath("$.[*].duree").value(hasItem(DEFAULT_DUREE)))
            .andExpect(jsonPath("$.[*].lieu").value(hasItem(DEFAULT_LIEU.toString())))
            .andExpect(jsonPath("$.[*].qualite").value(hasItem(DEFAULT_QUALITE.toString())))
            .andExpect(jsonPath("$.[*].videoUrl").value(hasItem(DEFAULT_VIDEO_URL.toString())));
    }

    @Test
    @Transactional
    public void getJournal() throws Exception {
        // Initialize the database
        journalRepository.saveAndFlush(journal);

        // Get the journal
        restJournalMockMvc.perform(get("/api/journals/{id}", journal.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(journal.getId().intValue()))
            .andExpect(jsonPath("$.cote").value(DEFAULT_COTE.toString()))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()))
            .andExpect(jsonPath("$.duree").value(DEFAULT_DUREE))
            .andExpect(jsonPath("$.lieu").value(DEFAULT_LIEU.toString()))
            .andExpect(jsonPath("$.qualite").value(DEFAULT_QUALITE.toString()))
            .andExpect(jsonPath("$.videoUrl").value(DEFAULT_VIDEO_URL.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingJournal() throws Exception {
        // Get the journal
        restJournalMockMvc.perform(get("/api/journals/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateJournal() throws Exception {
        // Initialize the database
        journalRepository.saveAndFlush(journal);
        journalSearchRepository.save(journal);
        int databaseSizeBeforeUpdate = journalRepository.findAll().size();

        // Update the journal
        Journal updatedJournal = journalRepository.findOne(journal.getId());
        updatedJournal
            .cote(UPDATED_COTE)
            .date(UPDATED_DATE)
            .duree(UPDATED_DUREE)
            .lieu(UPDATED_LIEU)
            .qualite(UPDATED_QUALITE)
            .videoUrl(UPDATED_VIDEO_URL);
        JournalDTO journalDTO = journalMapper.toDto(updatedJournal);

        restJournalMockMvc.perform(put("/api/journals")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(journalDTO)))
            .andExpect(status().isOk());

        // Validate the Journal in the database
        List<Journal> journalList = journalRepository.findAll();
        assertThat(journalList).hasSize(databaseSizeBeforeUpdate);
        Journal testJournal = journalList.get(journalList.size() - 1);
        assertThat(testJournal.getCote()).isEqualTo(UPDATED_COTE);
        assertThat(testJournal.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testJournal.getDuree()).isEqualTo(UPDATED_DUREE);
        assertThat(testJournal.getLieu()).isEqualTo(UPDATED_LIEU);
        assertThat(testJournal.getQualite()).isEqualTo(UPDATED_QUALITE);
        assertThat(testJournal.getVideoUrl()).isEqualTo(UPDATED_VIDEO_URL);

        // Validate the Journal in Elasticsearch
        Journal journalEs = journalSearchRepository.findOne(testJournal.getId());
        assertThat(journalEs).isEqualToComparingFieldByField(testJournal);
    }

    @Test
    @Transactional
    public void updateNonExistingJournal() throws Exception {
        int databaseSizeBeforeUpdate = journalRepository.findAll().size();

        // Create the Journal
        JournalDTO journalDTO = journalMapper.toDto(journal);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restJournalMockMvc.perform(put("/api/journals")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(journalDTO)))
            .andExpect(status().isCreated());

        // Validate the Journal in the database
        List<Journal> journalList = journalRepository.findAll();
        assertThat(journalList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteJournal() throws Exception {
        // Initialize the database
        journalRepository.saveAndFlush(journal);
        journalSearchRepository.save(journal);
        int databaseSizeBeforeDelete = journalRepository.findAll().size();

        // Get the journal
        restJournalMockMvc.perform(delete("/api/journals/{id}", journal.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean journalExistsInEs = journalSearchRepository.exists(journal.getId());
        assertThat(journalExistsInEs).isFalse();

        // Validate the database is empty
        List<Journal> journalList = journalRepository.findAll();
        assertThat(journalList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchJournal() throws Exception {
        // Initialize the database
        journalRepository.saveAndFlush(journal);
        journalSearchRepository.save(journal);

        // Search the journal
        restJournalMockMvc.perform(get("/api/_search/journals?query=id:" + journal.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(journal.getId().intValue())))
            .andExpect(jsonPath("$.[*].cote").value(hasItem(DEFAULT_COTE.toString())))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())))
            .andExpect(jsonPath("$.[*].duree").value(hasItem(DEFAULT_DUREE)))
            .andExpect(jsonPath("$.[*].lieu").value(hasItem(DEFAULT_LIEU.toString())))
            .andExpect(jsonPath("$.[*].qualite").value(hasItem(DEFAULT_QUALITE.toString())))
            .andExpect(jsonPath("$.[*].videoUrl").value(hasItem(DEFAULT_VIDEO_URL.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Journal.class);
        Journal journal1 = new Journal();
        journal1.setId(1L);
        Journal journal2 = new Journal();
        journal2.setId(journal1.getId());
        assertThat(journal1).isEqualTo(journal2);
        journal2.setId(2L);
        assertThat(journal1).isNotEqualTo(journal2);
        journal1.setId(null);
        assertThat(journal1).isNotEqualTo(journal2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(JournalDTO.class);
        JournalDTO journalDTO1 = new JournalDTO();
        journalDTO1.setId(1L);
        JournalDTO journalDTO2 = new JournalDTO();
        assertThat(journalDTO1).isNotEqualTo(journalDTO2);
        journalDTO2.setId(journalDTO1.getId());
        assertThat(journalDTO1).isEqualTo(journalDTO2);
        journalDTO2.setId(2L);
        assertThat(journalDTO1).isNotEqualTo(journalDTO2);
        journalDTO1.setId(null);
        assertThat(journalDTO1).isNotEqualTo(journalDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(journalMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(journalMapper.fromId(null)).isNull();
    }
}
