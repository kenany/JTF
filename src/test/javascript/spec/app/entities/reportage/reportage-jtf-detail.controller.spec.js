'use strict';

describe('Controller Tests', function() {

    describe('Reportage Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockReportage, MockJournal, MockPerson, MockMotCles;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockReportage = jasmine.createSpy('MockReportage');
            MockJournal = jasmine.createSpy('MockJournal');
            MockPerson = jasmine.createSpy('MockPerson');
            MockMotCles = jasmine.createSpy('MockMotCles');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Reportage': MockReportage,
                'Journal': MockJournal,
                'Person': MockPerson,
                'MotCles': MockMotCles
            };
            createController = function() {
                $injector.get('$controller')("ReportageJtfDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'jtfApp:reportageUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
